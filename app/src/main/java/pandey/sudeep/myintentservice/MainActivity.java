package pandey.sudeep.myintentservice;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private MovieAdapter adapter;

    private AppExecutors executors;
    private AppDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        layoutManager = new LinearLayoutManager(this);
        executors=new AppExecutors();
        database = AppDatabase.getInstance(this);

        Button viewButton = findViewById(R.id.viewButton);
        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView=findViewById(R.id.recyclerView);
                recyclerView.setHasFixedSize(true);
                recyclerView.setLayoutManager(layoutManager);

                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        final List<BestMovies> persistedMovies;
                        persistedMovies = database.getMovieDAO().loadAllMovies();
                        executors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                adapter = new MovieAdapter(persistedMovies);
                                recyclerView.setAdapter(adapter);
                            }
                        });
                    }
                });

            }
        });

        Button serviceButton = findViewById(R.id.startService);
        serviceButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                startForegroundService(new Intent(getApplicationContext(),MyIntentService.class));
            }
        });

        Button resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                executors.diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        database.getMovieDAO().deleteMovies();
                        executors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),"Table emptied..",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                });
            }
        });
    }
}
