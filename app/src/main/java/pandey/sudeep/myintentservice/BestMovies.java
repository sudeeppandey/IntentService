package pandey.sudeep.myintentservice;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName="bestmovies")
public class BestMovies {

    @PrimaryKey
    private int id;//shall we use long type instead?
    private String movieName;
    private String genre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public BestMovies(){

    }
    public BestMovies(int _id, String movie, String _genre){
        this.id=_id;
        this.movieName=movie;
        this.genre=_genre;
    }
}
