package pandey.sudeep.myintentservice;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import android.database.Cursor;

import java.util.List;

@Dao
public interface MovieDAO {

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertAllMovies(List<BestMovies> myMovies);

    @Insert(onConflict= OnConflictStrategy.REPLACE)
    void insertMovie(BestMovies myMovies);


    @Query("Select * from bestmovies")
    List<BestMovies> loadAllMovies();

    @Query("Select count(*) from bestmovies")
    int countRecords();

    @Update
    int updateMovie(BestMovies movie);

    @Query("Delete from bestmovies")
    int deleteMovies();

}
