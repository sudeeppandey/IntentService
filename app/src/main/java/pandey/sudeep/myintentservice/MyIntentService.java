package pandey.sudeep.myintentservice;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.List;

import static android.app.NotificationManager.IMPORTANCE_HIGH;


public class MyIntentService extends IntentService {

    private static final String TAG = MainActivity.class.getSimpleName();
    //private AppExecutors executors;
    private AppDatabase database;
    private static String CHANNEL_ID = "1";
    NotificationChannel channel;


    public MyIntentService() {
        super("MyIntentService");
        //setIntentRedelivery(true);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        database=AppDatabase.getInstance(this);

        //executors.diskIO().execute(new Runnable() {
            //@Override
           // public void run() {
                List<BestMovies> myMovies = DataGenerator.generateMovies();
                Intent notificationIntent = new Intent(this, MainActivity.class);
                PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);

                final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        channel = new NotificationChannel(CHANNEL_ID, name, IMPORTANCE_HIGH);
        channel.setDescription(description);
        // Register the channel with the system
        manager.createNotificationChannel(channel);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                .setContentTitle("Notifying U")
                .setContentText("Okay")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
       // manager.notify(1, mBuilder.build());

        startForeground(1,mBuilder.build());
                for (int i = 0; i < myMovies.size(); i++) {
                    database.getMovieDAO().insertMovie(myMovies.get(i));
                    Log.d(TAG, "Record number "+i+" Inserted in thread: "+Thread.currentThread().getName());
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                }
                Log.d(TAG, "Inserts Complete- "+Thread.currentThread().getName());
            }
        //});
    //}
}
